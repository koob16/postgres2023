def find_keys(**kwargs):
    # print(kwargs)
    # print(list(kwargs.keys()))
    # for i in list(kwargs.keys()):
    #     print(str(type(kwargs[i]))[8:-2] in ('list', 'tuple', 'set'))
    # print(list(filter(lambda x: type(kwargs[x]) in ('tuple', 'set'), list(kwargs.keys()))))
    return (sorted(filter(lambda x: str(type(kwargs[x]))[8:-2] in ('list', 'tuple'), list(kwargs.keys())), key=str.lower))

print(find_keys(t=[4, 5], W=[5, 3], A=(3, 2), a={2, 3}, b=[4]))