#### Описание/Пошаговая инструкция выполнения домашнего задания:
### Секционировать большую таблицу из демо базы flights

>*Сперва нам нужна база данных, загрузим дамп*

https://edu.postgrespro.ru/demo_big.zip

>*Посмотрим нашу базу, глянем на таблички и выберем самую большую*


![img.png](img/img.png)
```commandline
SELECT relname, relpages, reltuples 
FROM pg_class 
WHERE relname IN ('aircrafts','airports','boarding_passes','bookings', 'flights', 'seats', 'ticket_flights', 'tickets')
ORDER by 3 desc;
```
![img_1.png](img/img_1.png)

>*Подопытный найден* __ticket_flights__

> *Глянем что в ней есть*
```commandline
SELECT * FROM bookings.ticket_flights LIMIT 10;
```
![img_2.png](img/img_2.png)
>*Хм, интересно, класс обслуживания, а сколько их?*
```commandline
SELECT DISTINCT fare_conditions FROM bookings.ticket_flights;
```
![img_3.png](img/img_3.png)
>*Отлично, всего 3, вот по нему и разрежем нашу табличку*
><br>*Но сначала, соберём планы запросов и время по парочке селектов*
```commandline
EXPLAIN ANALYZE  
SELECT * FROM bookings.ticket_flights WHERE fare_conditions='Business';
```
![img_4.png](img/img_4.png)
```commandline
EXPLAIN ANALYZE  
SELECT * FROM bookings.ticket_flights WHERE fare_conditions IN ('Comfort','Economy');
```
![img_5.png](img/img_5.png)

>*Теперь начнём нарезку*
> <br>*Первым делом создадим новую табличку копию от* __ticket_flights__ *с партициями по* __fare_conditions__
```commandline
CREATE TABLE  bookings.ticket_flights_new
(
    ticket_no character(13) NOT NULL,
    flight_id integer NOT NULL,
    fare_conditions character varying(10) NOT NULL,
    amount numeric(10,2) NOT NULL,
    CONSTRAINT ticket_flights_amount_check CHECK ((amount >= (0)::numeric)),
    CONSTRAINT ticket_flights_fare_conditions_check CHECK (((fare_conditions)::text = ANY (ARRAY[('Economy'::character varying)::text, ('Comfort'::character varying)::text, ('Business'::character varying)::text])))
) PARTITION BY LIST (fare_conditions);
```
>*Далее нарежем 3 секции*
```commandline
CREATE TABLE bookings.ticket_flights_economy PARTITION OF bookings.ticket_flights_new FOR VALUES IN ('Economy');
CREATE TABLE bookings.ticket_flights_comfort PARTITION OF bookings.ticket_flights_new FOR VALUES IN ('Comfort');
CREATE TABLE bookings.ticket_flights_business PARTITION OF bookings.ticket_flights_new FOR VALUES IN ('Business');
```
![img_6.png](img/img_6.png)

>*А теперь перельём данные из старой таблички* __ticket_flights__ *в нашу заготовочку*
```commandline
INSERT INTO bookings.ticket_flights_new SELECT * FROM  bookings.ticket_flights;
```
>*Грохнем старую*
```commandline
DROP TABLE bookings.ticket_flights;
```
![img_7.png](img/img_7.png)
>*Ругается на ограничение, грохнем сначала его*
```commandline
ALTER TABLE bookings.boarding_passes  DROP CONSTRAINT boarding_passes_ticket_no_fkey;
DROP TABLE bookings.ticket_flights;
```
![img_8.png](img/img_8.png)
>*Теперь никаких конфликтов*

>*Переименуем нашу табличку* 
```commandline
ALTER TABLE bookings.ticket_flights_new RENAME TO ticket_flights;
```
>*Восстановим ограничения*
```commandline
ALTER TABLE bookings.ticket_flights
    ADD CONSTRAINT ticket_flights_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES bookings.flights(flight_id);
```
```commandline
ALTER TABLE bookings.ticket_flights
    ADD CONSTRAINT ticket_flights_ticket_no_fkey FOREIGN KEY (ticket_no) REFERENCES bookings.tickets(ticket_no);
```
>*А вот PK уже будем вешать на каждую секцию отдельно*
```commandline
ALTER TABLE ONLY bookings.ticket_flights_economy ADD CONSTRAINT ticket_flights_pkey_economy PRIMARY KEY (ticket_no, flight_id);
ALTER TABLE ONLY bookings.ticket_flights_comfort ADD CONSTRAINT ticket_flights_pkey_comfort PRIMARY KEY (ticket_no, flight_id);
ALTER TABLE ONLY bookings.ticket_flights_business ADD CONSTRAINT ticket_flights_pkey_business PRIMARY KEY (ticket_no, flight_id);
```

>*Теперь повторим наши запросики и посмотрим что получилось*
```commandline
EXPLAIN ANALYZE  
SELECT * FROM bookings.ticket_flights WHERE fare_conditions='Business';
```
![img_9.png](img/img_9.png)
>*Круто, почти в 8 раз быстрее*
```commandline
EXPLAIN ANALYZE  
SELECT * FROM bookings.ticket_flights WHERE fare_conditions IN ('Comfort','Economy');
```
![img_10.png](img/img_10.png)
>*Странно, разницы нет, стало даже чуть медленнее*