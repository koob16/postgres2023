* Создаем ВМ/докер c ПГ.
> *Ну всё по классике*
<br>![img.png](img/img.png)
```commandline
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y -q && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt -y install postgresql-16
```
![img_1.png](img/img_1.png)
* Создаем БД, схему и в ней таблицу.
* Заполним таблицы автосгенерированными 100 записями.
```commandline
create database otus;
\c otus;
create table student as 
select 
  generate_series(1,100) as id,
  md5(random()::text)::char(10) as fio;
```
![img_2.png](img/img_2.png)
* Под линукс пользователем Postgres создадим каталог для бэкапов
>*Зайдём в корень и создадим каталог* __bkup__
```commandline
cd /
mkdir bkup
```
>*Не забудем дать права пользователю* __postgres__ *на эту папку*
```commandline
chown -R postgres:postgres /bkup
```
![img_3.png](img/img_3.png)

* Сделаем логический бэкап используя утилиту COPY
```commandline
\copy student to '/bkup/st.sql';
```
* Восстановим в 2 таблицу данные из бэкапа.
>*Не всё так просто, сначала надо создать пустую табличку с той же структурой*
```commandline
create table student_new(id serial, fio text);
```
>*А потом смело грузим из бакапа данные*
```commandline
\copy student_new from '/bkup/st.sql'
```
![img_4.png](img/img_4.png)
* Используя утилиту pg_dump создадим бэкап в кастомном сжатом формате двух таблиц
```commandline
pg_dump -d otus --create -U postgres -Fc > /bkup/arh2.gz
```
![img_5.png](img/img_5.png)
* Используя утилиту pg_restore восстановим в новую БД только вторую таблицу!
>*Создадим пустую БД* __otus_new__
```commandline
create database otus_new;
```
>*и зальём туда данные из бакапа, но только табличку* __student_new__
```commandline
pg_restore -d otus_new -t student_new -U postgres /bkup/arh2.gz
```
>*Проверим, всё ли правильно получилось?*
<br>![img_6.png](img/img_6.png)
><br> *Всё чудненько!*