## Создать индексы на БД, которые ускорят доступ к данным.
### В данном задании тренируются навыки:

* определения узких мест
* написания запросов для создания индекса
оптимизации
Необходимо:
1. Создать индекс к какой-либо из таблиц вашей БД
2. Прислать текстом результат команды explain,
в которой используется данный индекс
3. Реализовать индекс для полнотекстового поиска
4. Реализовать индекс на часть таблицы или индекс
на поле с функцией
5. Создать индекс на несколько полей
6. Написать комментарии к каждому из индексов
7. Описать что и как делали и с какими проблемами
столкнулись


> *создадим базу и табличку в ней, заполним рандомными значениями*
```commandline
create table test as 
select generate_series as id
	, generate_series::text || (random() * 10)::text as col2 
    , (array['Yes', 'No', 'Maybe'])[floor(random() * 3 + 1)] as is_okay
from generate_series(1, 50000);
```
> *Сделаем селект и посмотрим сколько попугаев выдаёт*
```commandline
explain
select id from test where id = 1;
```
![img.png](img/img.png)
<br>
>*Подвесим обычный индекс на id и вновь прогоним explain*
```commandline
create index idx_test_id on test(id);

explain
select id from test where id = 1;
```
![img_1.png](img/img_1.png)
<br>
>*Попробуем поиск по тексту в табличке is_okay*
```commandline
explain
select is_okay from test where is_okay like '%e%';
```
![img_2.png](img/img_2.png)
<br>
>*Подвесим индекс полнотекстового поиска и ещё раз прогоним*
>*Сначала создадим доп поле, с типом tsvector*
```commandline
alter table test add column is_okay_g tsvector;
update test
set is_okay_g = to_tsvector(is_okay);
```
```commandline
explain
select is_okay from test where is_okay_g @@ to_tsquery('Yes');
```
![img_3.png](img/img_3.png)
>*Странно, но индекс не использовался*
###<br>

>*Подвесим индекс на часть данных*
><br>*Сначала прогоним селект без индекса*
```commandline
explain
select * from test where id < 30;
```
![img_4.png](img/img_4.png)
><br>*Подвесим индекс и снова прогоним селект*
```commandline
create index idx_test_id_50 on test(id) where id < 50;
```
![img_5.png](img/img_5.png)

<br>

>*Подвесим составной индекс на 2 поля*
```commandline
explain
select * from test where id = 1 and is_okay = 'True';
```
![img_6.png](img/img_6.png)
>*Создадим индексы*
```commandline
create index idx_test_id_is_okay on test(id, is_okay);
```
![img_7.png](img/img_7.png)
<br>
>*Просто прекрасно!*
### *Итоги:*
>*Индексы отлично ускоряют поиск данных, но стоит помнить, что индексы увеличивают вес базы, а так же тормозит вставку больших данных*