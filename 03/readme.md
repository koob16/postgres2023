* создать ВМ с Ubuntu 20.04/22.04 или развернуть докер любым удобным способом
> *создали на ЯО, дороговато вышло, ну на часок вполне*
![img_7.png](img/img_7.png)
* поставить на нем Docker Engine
```commandline
curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh && rm get-docker.sh && sudo usermod -aG docker $USER && newgrp docker
```
> *Всё быстренько сделалось*
* сделать каталог /var/lib/postgres
```
mkdir /var/lib/postgres
```
* развернуть контейнер с PostgreSQL 15 смонтировав в него /var/lib/postgresql
```commandline
docker network create pg-net
docker run --name pg-server --network pg-net -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v /var/lib/postgres:/var/lib/postgresql/data postgres:15
```
* развернуть контейнер с клиентом postgres
```commandline
docker run -it --rm --network pg-net --name pg-client postgres:15 psql -h pg-server -U postgres
```
* подключится из контейнера с клиентом к контейнеру с сервером и сделать таблицу с парой строк
```commandline
create table persons(id serial, first_name text, second_name text); 
insert into persons(first_name, second_name) values('ivan', 'ivanov'); 
insert into persons(first_name, second_name) values('petr', 'petrov'); 
```
![img_1.png](img/img_1.png)
* подключится к контейнеру с сервером с ноутбука/компьютера извне инстансов GCP/ЯО/места установки докера
```commandline
psql -p 5432 -U postgres -h 51.250.20.86 -d postgres -W
```
![img_2.png](img/img_2.png)
* удалить контейнер с сервером
![img_3.png](img/img_3.png)
```commandline
docker stop c343c92d5069
docker rm c343c92d5069
```
![img_4.png](img/img_4.png)
* создать его заново
```commandline
docker run --name pg-server --network pg-net -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v /var/lib/postgres:/var/lib/postgresql/data postgres:15
```
![img_5.png](img/img_5.png)
* подключится снова из контейнера с клиентом к контейнеру с сервером
```commandline
docker run -it --rm --network pg-net --name pg-client postgres:15 psql -h pg-server -U postgres
```
* проверить, что данные остались на месте
![img_6.png](img/img_6.png)

> *попрощаемся с виртуалочкой*
![img_8.png](img/img_8.png)

> *Довольно таки всё просто, вся база сохряняется во внешней директории а не в самом докере, так что удаление самого контейнера
совершенно не страшно* 
