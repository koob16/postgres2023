## Создать триггер для поддержки витрины в актуальном состоянии.

### Скрипт и развернутое описание задачи – в ЛК (файл hw_triggers.sql) или по ссылке: https://disk.yandex.ru/d/l70AvknAepIJXQ
#### В БД создана структура, описывающая товары (таблица goods) и продажи (таблица sales).
#### Есть запрос для генерации отчета – сумма продаж по каждому товару.
#### БД была денормализована, создана таблица (витрина), структура которой повторяет структуру отчета.
- Создать триггер на таблице продаж, для поддержки данных в витрине в актуальном состоянии (вычисляющий при каждой продаже сумму и записывающий её в витрину)
- Подсказка: не забыть, что кроме INSERT есть еще UPDATE и DELETE

#### Задание со звездочкой*
- Чем такая схема (витрина+триггер) предпочтительнее отчета, создаваемого "по требованию" (кроме производительности)?
- Подсказка: В реальной жизни возможны изменения цен.

> *Создадим базу* __otus__ *и наполним её данными из файла*
```commandline
DROP SCHEMA IF EXISTS pract_functions CASCADE;
CREATE SCHEMA pract_functions;

SET search_path = pract_functions, publ

-- товары:
CREATE TABLE goods
(
    goods_id    integer PRIMARY KEY,
    good_name   varchar(63) NOT NULL,
    good_price  numeric(12, 2) NOT NULL CHECK (good_price > 0.0)
);
INSERT INTO goods (goods_id, good_name, good_price)
VALUES 	(1, 'Спички хозайственные', .50),
		(2, 'Автомобиль Ferrari FXX K', 185000000.01);

-- Продажи
CREATE TABLE sales
(
    sales_id    integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    good_id     integer REFERENCES goods (goods_id),
    sales_time  timestamp with time zone DEFAULT now(),
    sales_qty   integer CHECK (sales_qty > 0)
);

INSERT INTO sales (good_id, sales_qty) VALUES (1, 10), (1, 1), (1, 120), (2, 1);
```

> *Отчёт*
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img.png](img/img.png)
> *Табличка, куда будут падать данные по триггерам*
```commandline
CREATE TABLE good_sum_mart
(
	good_name   varchar(63) NOT NULL,
	sum_sale	numeric(16, 2)NOT NULL
);
```

> *Создадим триггерную функцию*
```commandline
CREATE OR REPLACE FUNCTION pract_functions.sales_rewrite_trigger_function()
RETURNS trigger
AS
$$
BEGIN
--- Если триггер сработал для UPDATE или DELETE
IF TG_OP='UPDATE' OR TG_OP='DELETE' THEN
--- Отнимаем OLD.sales_qty*good_price по OLD.good_id
UPDATE pract_functions.good_sum_mart
SET sum_sale=sum_sale-
OLD.sales_qty*(SELECT good_price FROM pract_functions.goods WHERE goods.goods_id=OLD.good_id)
WHERE good_sum_mart.good_name=
(SELECT good_name FROM pract_functions.goods WHERE goods.goods_id=OLD.good_id);
END IF;
--- Если триггер сработал для UPDATE или INSERT
IF TG_OP='UPDATE' OR TG_OP='INSERT' THEN
--- Прибавляем NEW.sales_qty*good_price по NEW.good_id
INSERT INTO pract_functions.good_sum_mart (good_name, sum_sale)
VALUES (
(SELECT good_name FROM pract_functions.goods WHERE goods.goods_id=NEW.good_id),
NEW.sales_qty*(SELECT good_price FROM pract_functions.goods WHERE goods.goods_id=NEW.good_id))
ON CONFLICT (good_name) DO UPDATE
SET sum_sale=good_sum_mart.sum_sale+EXCLUDED.sum_sale;
END IF;
RETURN NULL;
END;
$$ LANGUAGE plpgsql;
```

> *Внесем изменение в таблицу* __good_sum_mart__ *- 
> сделаем поле* __good_name__ PRIMARY KEY, 
> *чтобы работала конструкция* INSERT ... DO UPDATE *в нашей функции*
> <br> *Сначала дропнем табличку и потом вновь создадим её*
```commandline
DROP TABLE pract_functions.good_sum_mart;
```
```commandline
CREATE TABLE pract_functions.good_sum_mart
(
good_name   varchar(63) PRIMARY KEY,
sum_sale numeric(16, 2) NOT NULL
);
```

> *Теперь подвесим триггеры*
```commandline
CREATE TRIGGER sales_insert_trigger
AFTER INSERT ON pract_functions.sales
FOR EACH ROW
EXECUTE PROCEDURE pract_functions.sales_rewrite_trigger_function();
```
```commandline
CREATE TRIGGER sales_update_trigger
AFTER UPDATE OF good_id, sales_qty ON pract_functions.sales
FOR EACH ROW
EXECUTE PROCEDURE pract_functions.sales_rewrite_trigger_function();
```
```commandline
CREATE TRIGGER sales_delete_trigger
AFTER DELETE ON pract_functions.sales
FOR EACH ROW
EXECUTE PROCEDURE pract_functions.sales_rewrite_trigger_function();
```

>*Проверим как работают*
> <br>*Сделаем DELETE * FROM sales, INSERT заново и сравним good_sum_mart и запрос SELECT*
```commandline
DELETE FROM pract_functions.sales;
INSERT INTO sales (good_id, sales_qty) VALUES (1, 10), (1, 1), (1, 120), (2, 1);
```
```commandline
SELECT * FROM pract_functions.good_sum_mart;
```
![img_1.png](img/img_1.png)
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img_2.png](img/img_2.png)

> *Добавим новую продажу*
```commandline
INSERT INTO sales (good_id, sales_qty) VALUES (1, 1);
```
> *И повторим запросы*
```commandline
SELECT * FROM pract_functions.good_sum_mart;
```
![img_3.png](img/img_3.png)
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img_4.png](img/img_4.png)

> *Удалим вновь добавленную строчку*
> <br> *Сначала найдём её в продажах и удалим по ID*
```commandline
SELECT * FROM sales;
```
![img_5.png](img/img_5.png)
```commandline
DELETE FROM sales WHERE sales_id=9;
```
> *Повторим наши запросы*
```commandline
SELECT * FROM pract_functions.good_sum_mart;
```
![img_6.png](img/img_6.png)
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img_7.png](img/img_7.png)

> *Увеличим количество продаж, спичек, например, опечатались, вместо 100 ввели 1, исправим это*
![img_8.png](img/img_8.png)
```commandline
UPDATE sales SET sales_qty=100 WHERE sales_id=6;
```
![img_9.png](img/img_9.png)
> *Повторим запросы*
```commandline
SELECT * FROM pract_functions.good_sum_mart;
```
![img_10.png](img/img_10.png)
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img_11.png](img/img_11.png)

> *А теперь ой, всё правильно было, товаров 1 только не спички, а тачки*
```commandline
UPDATE sales SET good_id=2, sales_qty=1 WHERE sales_id=6;
```
![img_12.png](img/img_12.png)

> *Вновь повторим запросы*
```commandline
SELECT * FROM pract_functions.good_sum_mart;
```
![img_13.png](img/img_13.png)
```commandline
SELECT G.good_name, sum(G.good_price * S.sales_qty)
FROM goods G
INNER JOIN sales S ON S.good_id = G.goods_id
GROUP BY G.good_name;
```
![img_14.png](img/img_14.png)

>*Урра!!! Всё работает как часики.*

<br>

#### Со *-ой
> *Если изменится цена товара, то обычный селект пересчитает всё по новым ценам*
> <br> *при добавлении новых продаж, в итоговой таблице они рассчитаются по новым ценам не изменяя старые данные*
> <br> *однако если мы изменим строчку продаж, то она также рассчитается в итоговой по новой цене, что тоже некорректно*
> <br> *для исключения такой ошибки расчётов, необходимо изменить структуру БД, и хранить старые цены, продажи привязывать к ценам через даты*