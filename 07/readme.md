# Настройка autovacuum с учетом особеностей производительности

### Цель:
* запустить нагрузочный тест pgbench
* настроить параметры autovacuum
* проверить работу autovacuum

#### Описание/Пошаговая инструкция выполнения домашнего задания:
* Создать инстанс ВМ с 2 ядрами и 4 Гб ОЗУ и SSD 10GB
> <br> *Создадим виртуалоку на Яндексе*
<br>![img.png](img/img.png)

* Установить на него PostgreSQL 15 с дефолтными настройками
> *Знаем что дефолтно ставится 14 версия, поэтому принудительно укажем что ставим 15 версию*
```commandline
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y -q && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt -y install postgresql-15
```
> *Проверим*
```commandline
pg_lsclusters
```
![img_1.png](img/img_1.png)
> *Да, всё верно, 15 версия кластера*
* Создать БД для тестов: выполнить 
```
pgbench -i postgres
```
![img_2.png](img/img_2.png)
* Запустить 
```
pgbench -c8 -P 6 -T 60 -U postgres postgres
```
![img_3.png](img/img_3.png)
* Применить параметры настройки PostgreSQL из прикрепленного к материалам занятия файла
```commandline
max_connections = 40
shared_buffers = 1GB
effective_cache_size = 3GB
maintenance_work_mem = 512MB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 500
random_page_cost = 4
effective_io_concurrency = 2
work_mem = 6553kB
min_wal_size = 4GB
max_wal_size = 16GB
```
> *Лезем в* __/etc/postgresql/15/main/postgresql.conf__ *и правим конфиг*
```commandline
nano /etc/postgresql/15/main/postgresql.conf 
```
> *И рестартанём кластер чтоб уж точно конфиг применился*
```commandline
systemctl restart postgresql
```
* Протестировать заново
<br>![img_4.png](img/img_4.png)
* Что изменилось и почему?
> *Изменив настройки кластера "по умолчанию", мы добились чуть лучшей производительности*
* Создать таблицу с текстовым полем и заполнить случайными или сгенерированными данным в размере 1млн строк
```commandline
CREATE TABLE student(

id serial,

fio char(100)

);
```
> *Создали табличку, теперь наполним её*
```commandline
INSERT INTO student(fio) SELECT 'noname' FROM generate_series(1,1000000);
```
* Посмотреть размер файла с таблицей
```commandline
SELECT pg_size_pretty(pg_total_relation_size('student'));
```
![img_5.png](img/img_5.png)
* 5 раз обновить все строчки и добавить к каждой строчке любой символ
```commandline
update student set fio = fio || '_1'; 
update student set fio = fio || '_2'; 
update student set fio = fio || '_3'; 
update student set fio = fio || '_4'; 
update student set fio = fio || '_5'; 
```
> *Просто проверим, обновились ли строчки?*
```commandline
select * from student where id = 1; 
```
![img_8.png](img/img_8.png)
> *Символы добавились*
* Посмотреть количество мертвых строчек в таблице и когда последний раз приходил автовакуум
```commandline
SELECT relname, n_live_tup, n_dead_tup, trunc(100*n_dead_tup/(n_live_tup+1))::float "ratio%", last_autovacuum FROM pg_stat_user_tables WHERE relname = 'student';
```
![img_6.png](img/img_6.png)
* Подождать некоторое время, проверяя, пришел ли автовакуум
![img_7.png](img/img_7.png)
> *Подчистил всех мертвяков*
* 5 раз обновить все строчки и добавить к каждой строчке любой символ
```commandline
update student set fio = fio || '_6'; 
update student set fio = fio || '_7'; 
update student set fio = fio || '_8'; 
update student set fio = fio || '_9'; 
update student set fio = fio || '_10'; 
```
![img_9.png](img/img_9.png)
* Посмотреть размер файла с таблицей
```commandline
SELECT pg_size_pretty(pg_total_relation_size('student'));
```
<br>![img_10.png](img/img_10.png)
> *Разжирела ))), ну ничего, просто запомним 539 МБ*
* Отключить Автовакуум на конкретной таблице
```commandline
ALTER TABLE student SET (autovacuum_enabled = off);
```
* 10 раз обновить все строчки и добавить к каждой строчке любой символ
```commandline
update student set fio = fio || '_11'; 
update student set fio = fio || '_12'; 
update student set fio = fio || '_13'; 
update student set fio = fio || '_14'; 
update student set fio = fio || '_15';
update student set fio = fio || '_16'; 
update student set fio = fio || '_17'; 
update student set fio = fio || '_18'; 
update student set fio = fio || '_19'; 
update student set fio = fio || '_20';  
```
* Посмотреть размер файла с таблицей
<br>![img_11.png](img/img_11.png)
* Объясните полученный результат
> *Почему так разжирела? Просто данные, хранятся в виде tuple, то есть неизменяемые, а значит update
> это не что иное, как пометка, что запись дохлая и создание новой записи*
><br>![img_12.png](img/img_12.png)
> <br>*10 млн записей - мёртвые и готовы к очистке*
* Не забудьте включить автовакуум)
```commandline
ALTER TABLE student SET (autovacuum_enabled = on);
```

#### Задание со *:
* Написать анонимную процедуру, в которой в цикле 10 раз обновятся все строчки в искомой таблице.
```commandline
DO $$
DECLARE
    i INTEGER;
BEGIN
    FOR i IN 1 .. 10
    LOOP
        RAISE NOTICE 'number of step %', i;
        EXECUTE ('update student set fio = fio || ' || i || ';');         
    END LOOP;
END $$;
```
* Не забыть вывести номер шага цикла.
<br>![img_13.png](img/img_13.png)
> *Ради интереса посмотрим результат*
<br>![img_14.png](img/img_14.png)
> <br>*Строчки обновились, но выборка была жесть какой долгой*
<br>![img_15.png](img/img_15.png)
> *и не удивительно*
><br> *Подождём* __avtovacuum__
<br>![img_16.png](img/img_16.png)
> <br>*уже легче дышать*
> <br> *А размер???*
<br>![img_17.png](img/img_17.png)
><br> *за счёт автовакуума, новые записи вставлялись в "очищенные от мертвичины ячейки", поэтому так сильно не разросся,
> но всё равно жирный*
> <br> *Сожмём табличку* __vacuum full__
```commandline
vacuum full student;
```
![img_18.png](img/img_18.png)
> *Ляпота!!! Однако, стоит помнить что для такой процедуры на дисковом пространстве должно быть достаточно места, так как она
создаёт новую идентичную пустую табличку и переписывает туда "живые" данные, и только потом грохает старую*
> <br>*Ну и наш селект теперь "летает"*