* создайте виртуальную машину c Ubuntu 20.04/22.04 LTS в GCE/ЯО/Virtual Box/докере
<br>![img.png](img/img.png)
* поставьте на нее PostgreSQL 15 через sudo apt
```commandline
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y -q && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt -y install postgresql-15
```
* проверьте что кластер запущен через 
```
sudo -u postgres pg_lsclusters
```
<br>![img_1.png](img/img_1.png)

* зайдите из под пользователя postgres в psql и сделайте произвольную таблицу с произвольным содержимым
```
postgres=# create table test(c1 text);
postgres=# insert into test values('1');
\q
```
<br>![img_2.png](img/img_2.png)

* остановите postgres например через
```
sudo -u postgres pg_ctlcluster 15 main stop
```
<br>![img_3.png](img/img_3.png)

* создайте новый диск к ВМ размером 10GB
<br>![img_4.png](img/img_4.png)
* добавьте свеже-созданный диск к виртуальной машине - надо зайти в режим ее редактирования и дальше выбрать пункт attach existing disk
<br>![img_5.png](img/img_5.png)
* проинициализируйте диск согласно инструкции и подмонтировать файловую систему, только не забывайте менять имя диска на актуальное, в вашем случае это скорее всего будет /dev/sdb - https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux
<br>![img_6.png](img/img_6.png)
<br>![img_7.png](img/img_7.png)
<br>![img_8.png](img/img_8.png)
<br>![img_9.png](img/img_9.png)
<br>![img_10.png](img/img_10.png)
<br>![img_11.png](img/img_11.png)
```commandline
nano /etc/fstab
```
> *вставим туда строку для автомонтирования*
```
/dev/vdb1 /mnt/data ext4 defaults 0 2
```
* перезагрузите инстанс и убедитесь, что диск остается примонтированным (если не так смотрим в сторону fstab)
```commandline
reboot
```
<br>![img_12.png](img/img_12.png)
> *на месте )))*

* сделайте пользователя postgres владельцем /mnt/data - 
```
chown -R postgres:postgres /mnt/data/
```
* перенесите содержимое /var/lib/postgres/15 в /mnt/data - 
```
mv /var/lib/postgresql/15 /mnt/data
```
* попытайтесь запустить кластер - 
```
sudo -u postgres pg_ctlcluster 15 main start
```
* напишите получилось или нет и почему
<br>![img_13.png](img/img_13.png)
> *не запускается, так как мы перенесли данные из директории*
* задание: найти конфигурационный параметр в файлах раположенных в /etc/postgresql/15/main который надо поменять и поменяйте его
```commandline
nano /etc/postgresql/15/main/postgresql.conf
```
* напишите что и почему поменяли
<br>![img_15.png](img/img_15.png)
> *меняем директорию данных*
* попытайтесь запустить кластер - 
```
sudo -u postgres pg_ctlcluster 15 main start
```
<br>![img_16.png](img/img_16.png)
> *а вот ничего подобного, не хочет стартовать, и пишет подсказку юзай __systemctl__ говорит*
* напишите получилось или нет и почему
<br>![img_18.png](img/img_18.png)
> *красота*
* зайдите через через psql и проверьте содержимое ранее созданной таблицы
<br>![img_19.png](img/img_19.png)
>*<br>данные на месте ))))*

* __задание со звездочкой *__: 
* не удаляя существующий инстанс ВМ сделайте новый
<br>![img_20.png](img/img_20.png)
> *подняли вторую машинку*
* поставьте на его PostgreSQL
<br>![img_21.png](img/img_21.png)
> *поставили, 15 версия*
* удалите файлы с данными из /var/lib/postgres
> *тормознём кластер и удалим данные*
```commandline
systemctl stop postgresql
```
<br>![img_22.png](img/img_22.png)

* перемонтируйте внешний диск который сделали ранее от первой виртуальной машины ко второй и запустите PostgreSQL на второй машине так чтобы он работал с данными на внешнем диске, расскажите как вы это сделали и что в итоге получилось.
> *отключили диск от одной машины, подключили к другой*
![img_23.png](img/img_23.png)
*посмотрим*
```commandline
lsblk
```
<br>![img_24.png](img/img_24.png)
> *есть такой
<br>создадим директорию* __/mnt/data__
```commandline
mkdir -p /mnt/data
```
> *замонтируем туда диск*
```commandline
mount -o defaults /dev/vdb1 /mnt/data
```
> *посмотрим что там
<br>![img_25.png](img/img_25.png)
<br>а там уже лежат данные
<br>внесём нужные изменения в конфиг файл
<br>![img_26.png](img/img_26.png)
<br>и стартанём кластер
<br>![img_27.png](img/img_27.png)
<br>проверим данные
<br>![img_28.png](img/img_28.png)
<br>всё на месте*

> *для полноты картины поправим* __fstab__ *,перезапустим машину и проверим, всё ли на месте?
<br>![img_29.png](img/img_29.png)
<br>всё на месте, отлично!!!*

