* На 1 ВМ создаем таблицы test для записи, test2 для запросов на чтение.
* Создаем публикацию таблицы test и подписываемся на публикацию таблицы test2 с ВМ №2.
* На 2 ВМ создаем таблицы test2 для записи, test для запросов на чтение.
* Создаем публикацию таблицы test2 и подписываемся на публикацию таблицы test1 с ВМ №1.
* 3 ВМ использовать как реплику для чтения и бэкапов (подписаться на таблицы из ВМ №1 и №2 ).

* *реализовать горячее реплицирование для высокой доступности на 4ВМ. Источником должна выступать ВМ №3. Написать с какими проблемами столкнулись.

<br>> *Начнём с классики. Развернём сразу 4 машины с постгресом.*
<br>![img.png](img/img.png)
<br> *Накатим на них 16 версию*
```commandline
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y -q && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt -y install postgresql-16
```
![img_1.png](img/img_1.png)
<br> *теперь в 1, 2 и 3 машине создадим БД* __otus__ *и 2 таблички* __test__ *и* __test2__
```commandline
create database otus;
\c otus
create table test(id serial, fio text);
create table test2(id serial, fio text);
```
>*Теперь изменим wal_level и создадим публикацию на 1 и 2 машине*
```
alter system set wal_level = replica;
```
> *На первой машине*
```
CREATE PUBLICATION test FOR TABLE test;
```
>*На второй машине*
```
CREATE PUBLICATION test2 FOR TABLE test2;
```
>*для пользователя postgres зададим пароль (у нас он будет 123qweASD)*
```
\password postgres
```
> *поправим файлы настроек* __pg_hba.conf__ __postgresql.conf__ 

>*Ну а теперь создадим подписки*
> *На второй машине*
```commandline
CREATE SUBSCRIPTION test 
CONNECTION 'host=10.129.0.8 port=5432 user=postgres password=123qweASD dbname=otus' 
PUBLICATION test WITH (copy_data = true);
```
> *На первой машине*
```commandline
CREATE SUBSCRIPTION test2 
CONNECTION 'host=10.129.0.13 port=5432 user=postgres password=123qweASD dbname=otus' 
PUBLICATION test2 WITH (copy_data = true);
```

>*На третьей машине делаем обе подписки*
```commandline
CREATE SUBSCRIPTION test3 
CONNECTION 'host=10.129.0.8 port=5432 user=postgres password=123qweASD dbname=otus' 
PUBLICATION test WITH (copy_data = true);
```
```
CREATE SUBSCRIPTION test4 
CONNECTION 'host=10.129.0.13 port=5432 user=postgres password=123qweASD dbname=otus' 
PUBLICATION test2 WITH (copy_data = true);
```
![img_4.png](img/img_4.png)
<br>
>*Глянем что на первой машине*
<br>![img_2.png](img/img_2.png)
<br>![img_5.png](img/img_5.png)
> *Глянем что на второй*
<br>![img_3.png](img/img_3.png)
<br>![img_6.png](img/img_6.png)
<br>*Глянем что на третьей*
> *Отличненько*
><br> *Теперь сделаем физическую реплику 3-ей машины на 4-ую*
><br> *Подготовим 3 тачку*
> <br>*Зададим пользователю postgres пароль и отредактим файлы конфига*
```commandline
\password postgres
```
```commandline
wal_level = hot_standby
hot_standby = on
```
>*postgresql.conf так же отредактим и на 4 машине*
><br>*не забудем и про pg_hba*
```commandline
host    replication     postgres        0.0.0.0/0               scram-sha-256
```
>*далее выгрузим бакап, не забудем указать ip хоста матера*
```commandline
pg_basebackup -P -R -X stream -c fast -h 10.129.0.25 -U postgres -D /bkup
```
>*переключимся на 4 машину, удалим main, и скопирнём туда выгрузившейся бакап*
><br>*переносил данные при помощи смонтированного диска*
><br>*Запустим кластер*
><br>*Проверим бегают-ли данные корректно*
><br>*внесём изменения в 1-ую табличку*
<br>![img_7.png](img/img_7.png)
><br>*Глянем на 2-ой, 3-ей и 4-ой*
<br>![img_8.png](img/img_8.png)
><br>*Отличненько!*
><br>*Теперь внесём изменения в 2 табличку на 2-ой машине*
<br>![img_9.png](img/img_9.png)
><br>*Проверим на 1-ой, 3-ей и 4-ой*
<br>![img_10.png](img/img_10.png)
><br>*Всё супер, всё работает как часики!*