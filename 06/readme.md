#### 1. создайте новый кластер PostgresSQL 14
> Создадим новую виртуалку, и накатим туда Postgres по умолчанию
```commandline
sudo apt install postgresql
```
> Узнаем версию
```commandline
pg_config --version
```
<br>![img.png](img/img_.png)<br>
> Как и положено 14 версия
#### 2. зайдите в созданный кластер под пользователем postgres
<br>![img.png](img/img1.png)<br>
#### 3. создайте новую базу данных testdb
```commandline
create database testnm;
```
<br>![img.png](img/img.png)<br>
#### 4. зайдите в созданную базу данных под пользователем postgres
```commandline
\c testnm;
```
<br>![img_1.png](img/img_1.png)<br>
#### 5. создайте новую схему testnm
```commandline
create schema testnm;
```
<br>![img_2.png](img/img_2.png)<br>
#### 6. создайте новую таблицу t1 с одной колонкой c1 типа integer
```commandline
create table t1(c1 integer);
```
<br>![img_3.png](img/img_3.png)<br>
#### 7. вставьте строку со значением c1=1
```commandline
insert into t1 values(1);
```
<br>![img_4.png](img/img_4.png)<br>

> Проверим ради интереса
<br>![img_5.png](img/img_5.png)<br>
#### 8. создайте новую роль readonly
```commandline
create role readonly;
```
<br>![img_6.png](img/img_6.png)<br>
#### 9. дайте новой роли право на подключение к базе данных testdb
```
grant connect on database testnm to readonly;
```
<br>![img_7.png](img/img_7.png)<br>
#### 10. дайте новой роли право на использование схемы testnm
```commandline
grant usage on schema testnm to readonly;
```
<br>![img_8.png](img/img_8.png)<br>
#### 11. дайте новой роли право на select для всех таблиц схемы testnm
```commandline
\ctestnm
\
```
<br>![img_9.png](img/img_9.png)<br>
#### 12. создайте пользователя testread с паролем test123
```commandline
create user testread with password 'test123';
```
<br>![img_10.png](img/img_10.png)<br>
#### 13. дайте роль readonly пользователю testread
```commandline
grant readonly to testread;
```
<br>![img_11.png](img/img_11.png)<br>
#### 14. зайдите под пользователем testread в базу данных testdb
```commandline
psql -U testread -d testnm -h 127.0.0.1 -W
```
<br>![img_12.png](img/img_12.png)<br>
#### 15. сделайте select * from t1;
<br>![img_13.png](img/img_13.png)<br>
#### 16. получилось? (могло если вы делали сами не по шпаргалке и не упустили один существенный момент про который позже)
> Нет не получилось, нет прав на табличку
#### 17. напишите что именно произошло в тексте домашнего задания
> Отстутвуют права доступа чтения на таблицу в базе
#### 18. у вас есть идеи почему? ведь права то дали?
> Дали-то дали, но ведь сама табличка создана в другой схеме
#### 19. посмотрите на список таблиц
<br>![img_14.png](img/img_14.png)<br>
> Воооот, схема-то другая, а у пользователя прав на эту схему нет
#### 20. подсказка в шпаргалке под пунктом 20
#### 21. а почему так получилось с таблицей (если делали сами и без шпаргалки то может у вас все нормально)
> Таблицу создавали по умолчанию, в public, а что бы по умолчанию создавалась в другой схеме, надо эту схему сделать по умолчанию
#### 22. вернитесь в базу данных testdb под пользователем postgres
```commandline
sudo -u postgres psql
```
#### 23. удалите таблицу t1
```commandline
\c testnm;
drop table t1;
```
#### 24. создайте ее заново но уже с явным указанием имени схемы testnm
```commandline
create table testnm.t1(c1 integer);
```
#### 25. вставьте строку со значением c1=1
```commandline
insert into testnm.t1 values(1);
```
#### 26. зайдите под пользователем testread в базу данных testdb
```commandline
psql -U testread -d testnm -h 127.0.0.1 -W
```
#### 27. сделайте select * from testnm.t1;
#### 28. получилось?
<br>![img_15.png](img/img_15.png)<br>
> <br>Не получилось!!!
#### 29. есть идеи почему? если нет - смотрите шпаргалку
> После пересоздания таблицы, нужно и права на эту табличку заново назначить
#### 30. как сделать так чтобы такое больше не повторялось? если нет идей - смотрите шпаргалку
> Пересоздать права на таблички схемы
> Или сделать так чтоб права назначались автоматически для вновь создаваемых таблиц
```commandline
ALTER DEFAULT PRIVILEGES IN SCHEMA testnm GRANT SELECT ON TABLES TO testread;
```
<br>![img_17.png](img/img_17.png)<br>
#### 31. сделайте select * from testnm.t1;
#### 32. получилось?
<br>![img_18.png](img/img_18.png)<br>
> <br>Получилось
#### 33. есть идеи почему? если нет - смотрите шпаргалку
> Так перераздали права на select в схеме testnm в базе testnm
#### 34. сделайте select * from testnm.t1;
#### 35. получилось?
#### 36. ура!
#### 37. теперь попробуйте выполнить команду create table t2(c1 integer); insert into t2 values (2);
<br>![img_19.png](img/img_19.png)<br>
> <br>ДА!!! Создались без проблем!
#### 38. а как так? нам же никто прав на создание таблиц и insert в них под ролью readonly?
> По умолчанию у всех пользователей есть право создания в схеме puplic
#### 39. есть идеи как убрать эти права? если нет - смотрите шпаргалку
> В базе нужно отозвать права у всех пользователей использовать схему public
```commandline
REVOKE ALL ON schema public FROM PUBLIC;
```
<br>![img_20.png](img/img_20.png)<br>
#### 40. если вы справились сами то расскажите что сделали и почему, если смотрели шпаргалку - объясните что сделали и почему выполнив указанные в ней команды
#### 41. теперь попробуйте выполнить команду create table t3(c1 integer); insert into t2 values (2);
<br>![img_22.png](img/img_22.png)<br>
<br>![img_21.png](img/img_21.png)<br>
#### 42. расскажите что получилось и почему
> Теперь, когда права на public в базе отобраны для всех пользователей, доступа на создание по умолчанию в схеме public уже нет
> и на вставку тоже и вообще всего что в схеме.