* Настройте выполнение контрольной точки раз в 30 секунд.
> *Идём в postgresql.conf и выставляем*
```
checkpoint_timeout = 30
```
* 10 минут c помощью утилиты pgbench подавайте нагрузку.
```
pgbench -i postgres
pgbench -T 600 -U postgres postgres
```
<br>![img.png](img/img.png)
* Измерьте, какой объем журнальных файлов был сгенерирован за это время. 
Оцените, какой объем приходится в среднем на одну контрольную точку.
<br>![img_1.png](img/img_1.png)
<br>![img_2.png](img/img_2.png)
<br>![img_3.png](img/img_3.png)
> *В среднем выходит по 1мб на контрольную точку*
* Проверьте данные статистики: все ли контрольные точки выполнялись точно по расписанию. 
Почему так произошло?
> *Нет, не все, если данные не менялись, то и checkpoint делать не нужно*
* Сравните tps в синхронном/асинхронном режиме утилитой pgbench. Объясните полученный результат.
> *Посмотрим, включен-ли синхронный режим* 
<br>![img_4.png](img/img_4.png)
<br>*теперь запустим нагрузку на минутку*
```commandline
pgbench -T 60 -U postgres postgres
```
<br>![img_5.png](img/img_5.png)
><br>*отключим синхронный режим*
<br>![img_6.png](img/img_6.png)
><br> *проверим*
<br>![img_7.png](img/img_7.png)
><br> *отключен, запустим нагрузку ещё раз*
<br>![img_8.png](img/img_8.png)
><br>*ВАУ!!! а всё почему, а потому что игнорим подтверждение сбросилось на диск или нет*
* Создайте новый кластер с включенной контрольной суммой страниц. Создайте таблицу. 
Вставьте несколько значений. Выключите кластер. Измените пару байт в таблице. 
Включите кластер и сделайте выборку из таблицы. Что и почему произошло? 
как проигнорировать ошибку и продолжить работу?
><br>*Остановим кластер*
```commandline
systemctl stop postgresql
```
><br>*Включим проверку контрольных сумм*
```commandline
su postgres -c '/usr/lib/postgresql/15/bin/pg_checksums --enable -D "/var/lib/postgresql/15/main"' 
```
<br>![img_9.png](img/img_9.png)
><br>*Запустим кластер*
```commandline
systemctl start postgresql
```
><br>*Создадим табличку и вставим пару значений*
```commandline
create table persons(id serial, first_name text, second_name text); 
insert into persons(first_name, second_name) values('ivan', 'ivanov'); 
insert into persons(first_name, second_name) values('petr', 'petrov'); 
```
><br>*посмотрим где лежит наша табличка*
<br>![img_10.png](img/img_10.png)
><br>*Зайдём туда и добавим пару байтов*
><br>*Запустим кластер и сделаем селект*
<br>![img_11.png](img/img_11.png)
><br>*Чтоб проигнорить ошибку и продолжить работу*
```commandline
SET ignore_checksum_failure = on;
```
<br>![img_12.png](img/img_12.png)