* развернуть виртуальную машину любым удобным способом
<br>![img.png](img/img.png)
> *Всё по классике, Яндекс - наше родное*
* поставить на неё PostgreSQL 15 любым способом
```commandline
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y -q && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt -y install postgresql-15
```
![img_1.png](img/img_1.png)

* настроить кластер PostgreSQL 15 на максимальную производительность не обращая внимание на возможные проблемы с надежностью в случае аварийной перезагрузки виртуальной машины
* нагрузить кластер через утилиту через утилиту pgbench (https://postgrespro.ru/docs/postgrespro/14/pgbench)
* написать какого значения tps удалось достичь, показать какие параметры в какие значения устанавливали и почему

> *Сначала сделаем тест нагрузок по дефолту*
<br>![img_2.png](img/img_2.png)

> *Выставим настройку с pgtune.leopard.in.ua*
<br>![img_4.png](img/img_4.png)
<br>![img_3.png](img/img_3.png)
<br>![img_5.png](img/img_5.png)
> <br> *Да, не густо*
```commandline
# DB Version: 15
# OS Type: linux
# DB Type: oltp
# Total Memory (RAM): 4 GB
# CPUs num: 2
# Connections num: 100
# Data Storage: ssd

max_connections = 100
shared_buffers = 1GB
effective_cache_size = 3GB
maintenance_work_mem = 256MB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 1.1
effective_io_concurrency = 200
work_mem = 5242kB
huge_pages = off
min_wal_size = 2GB
max_wal_size = 8GB
```
![img_6.png](img/img_6.png)
> *Для OLTP уже лучше, ну а теперь отключим синхронизацию, так как по условиям надёжность можно игнорить* 
```commandline
fsync = off
synchronous_commit = off
```
![img_7.png](img/img_7.png)
><br>*Огогошеньки!!!!*

* Задание со *: 
аналогично протестировать через утилиту https://github.com/Percona-Lab/sysbench-tpcc (требует установки
https://github.com/akopytov/sysbench)
> *Вот тут печаль печальная, и скачал и поставил, а вот запустить никак не смог, постоянно какието ошибки валились, и не пойму что не так делаю*
<br>![img_8.png](img/img_8.png)