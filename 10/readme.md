* Настройте сервер так, чтобы в журнал сообщений сбрасывалась информация о блокировках, 
удерживаемых более 200 миллисекунд. 
Воспроизведите ситуацию, при которой в журнале появятся такие сообщения.
> *Выставим в* __postgresql.conf__ *значение* __lock_timeout = 200__
> *Создадим базу в которой будем работать*
```commandline
create database locks;
```
> *и табличку в ней с пару тройкой записей*
```commandline
CREATE TABLE accounts(
  acc_no integer PRIMARY KEY,
  amount numeric
);
INSERT INTO accounts VALUES (1,1000.00), (2,2000.00), (3,3000.00);
```
> *начнём транзакцию и обновим одну строчку*
```commandline
BEGIN;
UPDATE accounts SET amount = amount + 100 WHERE acc_no = 1;
```
> *откроем вторую сессию и тоже попробуем обновить эту же строчку*
```commandline
BEGIN;
UPDATE accounts SET amount = amount + 100 WHERE acc_no = 1;
```
<br>![img.png](img/img.png)
<bd>
> *глянем что у нас попало в лог*
<br>![img_1.png](img/img_1.png)
> *по таймауту ожидания, транзакция второй сессии откатилась и соответствующая запись попала в лог субд*
><br> *Откатим транзакции*
```commandline
ROLLBACK;
```
> *Вернём настройку* __postgresql.conf__ *в дефолтное состояние*

* Смоделируйте ситуацию обновления одной и той же строки тремя командами UPDATE в разных сеансах. 
Изучите возникшие блокировки в представлении pg_locks и убедитесь, что все они понятны. 
Пришлите список блокировок и объясните, что значит каждая.
> *Сначала надстроим вьюху над представлением.* 
```commandline
CREATE VIEW locks_v AS
SELECT pid,
       locktype,
       CASE locktype
         WHEN 'relation' THEN relation::regclass::text
         WHEN 'transactionid' THEN transactionid::text
         WHEN 'tuple' THEN relation::regclass::text||':'||tuple::text
       END AS lockid,
       mode,
       granted
FROM pg_locks
WHERE locktype in ('relation','transactionid','tuple')
AND (locktype != 'relation' OR relation = 'accounts'::regclass);
```
>*Во-первых, сделаем вывод чуть более компактным.* 
><br>*Во-вторых, ограничимся только интересными блокировками:* 
><br>*(фактически, отбрасываем блокировки виртуальных номеров транзакций, индекса на таблице* __accounts__, __pg_locks__ *и самого представления — в общем, всего того, что не имеет отношения к делу и только отвлекает).*
><br>*Теперь начнем первую транзакцию и обновим строку.*
```
BEGIN;
SELECT txid_current(), pg_backend_pid();

UPDATE accounts SET amount = amount + 100.00 WHERE acc_no = 1;
```
```commandline
SELECT * FROM locks_v WHERE pid = 187352;
```
<br>![img_2.png](img/img_2.png)
><br>*Транзакция удерживает блокировку таблицы и собственного номера. Пока все ожидаемо.*
><br>*Начинаем вторую транзакцию и пытаемся обновить ту же строку.*
```commandline
BEGIN;
SELECT txid_current(), pg_backend_pid();
UPDATE accounts SET amount = amount + 100.00 WHERE acc_no = 1;
```
<br>![img_3.png](img/img_3.png)
```commandline
SELECT * FROM locks_v WHERE pid = 187619;
```
<br>![img_4.png](img/img_4.png)
>*Помимо блокировки таблицы и собственного номера, мы видим еще две блокировки. Вторая транзакция обнаружила, что строка заблокирована первой и «повисла» на ожидании ее номера (granted = f). Но откуда и зачем взялась блокировка версии строки (locktype = tuple)?*
><br>*Начнём 3-ю:*
<br>![img_5.png](img/img_5.png)
<br>![img_6.png](img/img_6.png)
>*Попыталась захватить блокировку версии строки и повисла уже на этом шаге.*
><br>*Глянем общую картину*
```commandline
SELECT pid, wait_event_type, wait_event, pg_blocking_pids(pid)
FROM pg_stat_activity
WHERE backend_type = 'client backend' ORDER BY pid;
```
<br>![img_7.png](img/img_7.png)
>*Получается своеобразная «очередь», в которой есть первый (тот, кто удерживает блокировку версии строки) и все остальные, выстроившиеся за первым.*

* Воспроизведите взаимоблокировку трех транзакций. Можно ли разобраться в ситуации постфактум, изучая журнал сообщений?

Session #1
>*Первая транзакция намерена перенести 100 рублей с первого счета на второй. Для этого она сначала уменьшает первый счет*
```
BEGIN;
UPDATE accounts SET amount = amount - 100.00 WHERE acc_no = 1;
```

Session #2
>*В это же время вторая транзакция намерена перенести 10 рублей со второго счета на третий. Она начинает с того, что уменьшает второй счет:*
```
BEGIN;
UPDATE accounts SET amount = amount - 10.00 WHERE acc_no = 2;
```

Session #3
>*В это же время третья транзакция намерена перенести 5 рублей со третьего счета на первый. Она начинает с того, что уменьшает третий счет:*
```
BEGIN;
UPDATE accounts SET amount = amount - 5.00 WHERE acc_no = 3;
```

Session #1
```
UPDATE accounts SET amount = amount + 100.00 WHERE acc_no = 2;
```

Session #2
```
UPDATE accounts SET amount = amount + 10.00 WHERE acc_no = 3;
```

Session #3
```
UPDATE accounts SET amount = amount + 5.00 WHERE acc_no = 1;
```
>*Возникает циклическое ожидание, который никогда не завершится само по себе. Через секунду первая транзакция, не получив доступ к ресурсу, инициирует проверку взаимоблокировки и обрывается сервером.*
<br>![img_8.png](img/img_8.png)
<br>![img_9.png](img/img_9.png)
<br>![img_10.png](img/img_10.png)
><br>*Заглянем в лог*
<br>![img_11.png](img/img_11.png)
>*Вот и видим запись о взаимоблокировке, кто кого и какие транзакции*


* Могут ли две транзакции, выполняющие единственную команду UPDATE одной и той же таблицы (без where), заблокировать друг друга?
>*Да могут*
><br>*Во время беседы, Евгений имел неосторожность обронить, что знает 7 способов устроить* __deadlock__ *в таком варианте*
* Задание со звездочкой*:
Попробуйте воспроизвести такую ситуацию.
>*Воспроизведём одну из таких ситуаций*
><br>*Создадим табличку test и заполним её данными*
```commandline
create table test(id integer primary key generated always as identity, n float);
insert into test(n) select random() from generate_series(1,1000000);
```
>*Теперь одновременно запустим в 2-х сессиях запросы update*

Session #1
```commandline
UPDATE test SET n = (select id from test order by id asc limit 1 for update);
```

Session #2
```commandline
UPDATE test SET n = (select id from test order by id desc limit 1 for update);
```
>*В первой сессии транзакция была отброшена по ошибке взаимоблокировки*
<br>![img_12.png](img/img_12.png)
><br>*Ну и по классике глянем лог*
<br>![img_13.png](img/img_13.png)
