* написания запросов с различными типами соединений
Необходимо:
1. Реализовать прямое соединение двух или более таблиц
2. Реализовать левостороннее (или правостороннее)
соединение двух или более таблиц
3. Реализовать кросс соединение двух или более таблиц
4. Реализовать полное соединение двух или более таблиц
5. Реализовать запрос, в котором будут использованы
разные типы соединений
6. Сделать комментарии на каждый запрос
7. К работе приложить структуру таблиц, для которых
выполнялись соединения
#### Задание со звездочкой*
* Придумайте 3 своих метрики на основе показанных представлений, отправьте их через ЛК, а так же поделитесь с коллегами в слаке

>*Сперва нам нужна база данных, создадим ей и загрузим дамп*

[aeroflot.sql](sources/aeroflot.sql)

![img_1.png](img/img_1.png)

#### Реализовать прямое соединение двух или более таблиц
>*top 5 городов*
```commandline
select a.aircraft_code, dep.city departure, arr.city arrive, count(*)
from bookings.aircrafts a
         join bookings.flights f on a.aircraft_code = f.aircraft_code
         join bookings.airports dep on dep.airport_code = f.departure_airport
         join bookings.airports arr on arr.airport_code = f.departure_airport
group by 1, 2, 3
order by 4 desc
limit 5
```
<br>![img_2.png](img/img_2.png)

#### Реализовать левостороннее (или правостороннее) соединение двух или более таблиц.
Реализовать запрос, в котором будут использованы
разные типы соединений
>*заполняемость при перелетах*
```commandline
select f.flight_id, count(tf.*) / count(s.*) * 100 from bookings.flights f
join bookings.aircrafts a on f.aircraft_code = a.aircraft_code
join bookings.seats s on a.aircraft_code = s.aircraft_code
left join bookings.ticket_flights tf on f.flight_id = tf.flight_id
group by 1
```
![img_3.png](img/img_3.png)

#### Реализовать кросс соединение двух или более таблиц
#### Реализовать полное соединение двух или более таблиц
>*самые дальние друг от друга аэропорты*
```commandline
select a.city, b.city, 111.111 *
    DEGREES(ACOS(LEAST(1.0, COS(RADIANS(a.Latitude))
         * COS(RADIANS(b.Latitude))
         * COS(RADIANS(a.Longitude - b.Longitude))
         + SIN(RADIANS(a.Latitude))
         * SIN(RADIANS(b.Latitude))))) as distance from bookings.airports a
cross join bookings.airports b
order by distance desc
limit 1
```
![img_4.png](img/img_4.png)

### Задание со *
#### Метрики
>*Топ самых быстро меняемых табличек которые скорее всего потребуют отдельной настройки для автовакуума*
```commandline
select n_mod_since_analyze + n_ins_since_vacuum / EXTRACT(EPOCH FROM (now() - last_autovacuum)), relname from pg_stat_user_tables
where last_autovacuum is not null
order by 1 desc
```
![img_5.png](img/img_5.png)

>*pid'ы которые скорее всего в скором времени завесят базу*
```commandline
select
EXTRACT(EPOCH FROM (now() - state_change)) as idle_seconds,
pid
from pg_stat_activity
where state = 'idle' and EXTRACT(EPOCH FROM (now() - state_change)) > 60
```
![img_6.png](img/img_6.png)

>*какая из баз более нагруженна на запись, а какая на чтение*
```commandline
select
(tup_inserted + tup_deleted + tup_updated) / EXTRACT(EPOCH FROM (now() - stats_reset)) writes_per_second,
tup_returned / EXTRACT(EPOCH FROM (now() - stats_reset)) reads_per_second,
datname
from pg_stat_database
```
![img_7.png](img/img_7.png)
>*Замазал наименование баз*