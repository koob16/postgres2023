# Тема: «Построения отказоустойчивого кластера PostgreSQL на базе Patroni для систем 1С:Предприятие.»

Начнём с подготовки!
* Потребуется: 
    * 4 ВМ для СУБД, 
    * 1 ВМ для точки входа, 
    * 1 ВМ для сервера 1С

PS. В контуре уже есть кластер 1с, его и будем использовать на последнем этапе

VM1, VM2, VM3, VM4 - Сервера СУБД

VM5 - Сервер, будет выступать точкой входа

Характеристики у всех машин одинаковые:
```commandline
Linux version 6.1.50-1-generic 
(devos-ci@runner-v-w6stxqz-project-81-concurrent-0) 
(gcc-8 (AstraLinuxSE 8.3.0-6) 8.3.0, GNU ld (GNU Binutils for AstraLinux) 2.31.1) 

cpu:                                                            
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz, 2095 MHz
                       
H/W path      Device      Class      Description
================================================
                          system     Virtual Machine (None)
/0                        bus        Virtual Machine
/0/0                      memory     1MiB BIOS
/0/4                      processor  Intel(R) Xeon(R) Silver 4216 CPU @ 2.10GHz
/0/6                      memory     15GiB System Memory
/0/6/0                    memory     3968MiB 
/0/6/1                    memory     11GiB 
/0/1                      system     PnP device PNP0b00
/0/2          scsi0       storage    
/0/2/0.0.0    /dev/sda    disk       53GB Virtual Disk
/0/2/0.0.0/1              volume     511MiB Windows FAT volume
/0/2/0.0.0/2  /dev/sda2   volume     48GiB EXT4 volume
/0/2/0.0.0/3  /dev/sda3   volume     975MiB Linux swap volume
/0/2/0.0.1    /dev/cdrom  disk       Virtual DVD-ROM
/0/2/0.0.1/0  /dev/cdrom  disk       
/1            eth0        network    Ethernet interface

Файловая система Размер Использовано  Дост Использовано% Cмонтировано в
udev               7,6G            0  7,6G            0% /dev
tmpfs              1,6G          60M  1,5G            4% /run
/dev/sda2           48G         3,4G   42G            8% /
tmpfs              7,6G          64K  7,6G            1% /dev/shm
tmpfs              5,0M            0  5,0M            0% /run/lock
/dev/sda1          511M         296K  511M            1% /boot/efi
tmpfs              1,6G            0  1,6G            0% /run/user/1001

```
8 ядер, 16 гиг оперативы и 50 гиг дискового пространства

### PostgreSQL

начнём подготовку:

у меня уже есть пакет PostgreSQL14 от 1С, его и накатим на VM1-4

стащим пакеты на машинки (пакеты в папке postgres14), и начнём установку

```commandline
-переходим в папку где пакеты лежат
cd postgres/

-Устанавливаем собранные пакеты
apt --no-install-recommends install ./libpq5_14.5-3.1C_amd64.deb
apt --no-install-recommends install ./postgresql-client-common_246.pgdg100+1_all.deb
apt --no-install-recommends install ./postgresql-common_246.pgdg100+1_all.deb
apt --no-install-recommends install ./postgresql-client-14_14.5-3.1C_amd64.deb
apt --no-install-recommends install ./postgresql-14_14.5-3.1C_amd64.deb
```

на этом и закончим, все остальные настройки привяжем из Patroni

### etcd

теперь соберём кластер ectd

для этого на каждой машине VM1-5 установим:
```commandline
apt install etcd 
```
теперь плотно займёмся VM5

```commandline
systemctl stop etcd
```
уберём файл настроек, впишем свой
```commandline
mv /etc/default/etcd /etc/default/etcd.def3
nano /etc/default/etcd 
```
```commandline
ETCD_NAME="VM5"
ETCD_DATA_DIR="/var/lib/etcd/pg-cluster"
ETCD_LISTEN_PEER_URLS="http://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.1.31.128:2380"
ETCD_INITIAL_CLUSTER="VM5=http://10.1.31.128:2380"
ETCD_INITIAL_CLUSTER_STATE="existing"
ETCD_INITIAL_CLUSTER_TOKEN="pg-cluster"
ETCD_ADVERTISE_CLIENT_URLS="http://10.1.31.128:2379"
```
стартанём etcd

```commandline
systemctl start etcd
```
теперь будем добавлять узлы в существующий кластер

начнём с VM1, на VM5 пишем
```commandline
etcdctl member add VM1 --peer-urls=http://10.1.31.124:2380 
```
переходим к VM1 и правим конфиг

```commandline
ETCD_NAME="VM1"
ETCD_DATA_DIR="/var/lib/etcd/pg-cluster"
ETCD_LISTEN_PEER_URLS="http://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.1.31.124:2380"
ETCD_INITIAL_CLUSTER="VM5=http://10.1.31.128:2380,VM1=http://10.1.31.124:2380"
ETCD_INITIAL_CLUSTER_STATE="existing"
ETCD_INITIAL_CLUSTER_TOKEN="pg-cluster"
ETCD_ADVERTISE_CLIENT_URLS="http://10.1.31.124:2379"
```

запускаем etcd

```commandline
systemctl restart etcd
```

возвращаемся к VM5 и добавляем VM2:
```commandline
etcdctl member add VM2 --peer-urls=http://10.1.31.125:2380 
```

идём на VM2, правим конфиг:
```commandline
ETCD_NAME="VM2"
ETCD_DATA_DIR="/var/lib/etcd/pg-cluster"
ETCD_LISTEN_PEER_URLS="http://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.1.31.125:2380"
ETCD_INITIAL_CLUSTER="VM2=http://10.1.31.125:2380,VM5=http://10.1.31.128:2380,VM1=http://10.1.31.124:2380"
ETCD_INITIAL_CLUSTER_STATE="existing"
ETCD_INITIAL_CLUSTER_TOKEN="pg-cluster"
ETCD_ADVERTISE_CLIENT_URLS="http://10.1.31.125:2379"
```

аналогично поступаем с VM3 и VM4
меняя в конфиге
```commandline
ETCD_NAME="VM2" # тут пишем имя машины VM1-5
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.1.31.125:2380" # её IP
ETCD_INITIAL_CLUSTER= # тут в итоге будет список всех машин в кластере
ETCD_ADVERTISE_CLIENT_URLS="http://10.1.31.125:2379" # тоже IP машины где лежит файл
```
в итоге у всех будет примерно одинаковый конфиг, с разницей в имени и ip

```commandline
ETCD_NAME="VM1"
ETCD_DATA_DIR="/var/lib/etcd/pg-cluster"
ETCD_LISTEN_PEER_URLS="http://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.1.31.124:2380"
ETCD_INITIAL_CLUSTER="VM2=http://10.1.31.125:2380,VM3=http://10.1.31.126:2380,VM4=http://10.1.31.127:2380,VM5=http://10.1.31.128:2380,VM1=http://10.1.31.124:2380"
ETCD_INITIAL_CLUSTER_STATE="existing"
ETCD_INITIAL_CLUSTER_TOKEN="pg-cluster"
ETCD_ADVERTISE_CLIENT_URLS="http://10.1.31.124:2379"
```

перезапустим etcd на всех машинках

идём на VM5, создадим пользователя

```commandline
etcdctl user add root 
etcdctl user grant-role root root 
etcdctl user get root
etcdctl auth enable 
```

проверим наш кластер
```commandline
etcdctl member list
```
![img.png](img/img.png)

```commandline
etcdctl --user=root --cluster=true endpoint health 
```
![img_1.png](img/img_1.png)

все узлы на месте и здоровы, на этом закончим и перейдём к Patroni

## Patroni

для него сначала накатим Python, а потом уже подгрузим patroni

делаем на всех СУБД, VM1-4

так как у нас машинки в корпоративной среде без прямого выхода в инет, явно укажем прокси
```commandline
apt install python3 gcc python3-dev python3-pip python3-etcd
python3 -m pip install --proxy http://10.1.31.103:3128 --upgrade pip
python3 -m pip install --proxy http://10.1.31.103:3128 psycopg2-binary 
python3 -m pip install --proxy http://10.1.31.103:3128 patroni[etcd] 
```

далее создадим каталог и сделаем конфиг для patroni

```commandline
mkdir /etc/patroni
chown -R postgres:postgres /etc/patroni/
chmod 755 /etc/patroni/
nano /etc/patroni/patroni.yml
```
для VM1 конфиг будет таким
```commandline
scope: pgsql
namespace: /cluster/
name: VM1

restapi:
    listen: 10.1.31.124:8008
    connect_address: 10.1.31.124:8008

etcd:
    hosts: 10.1.31.124:2379,10.1.31.125:2379,10.1.31.126:2379,10.1.31.127:2379,10.1.31.128:2379

# this section (bootstrap) will be written into Etcd:/<namespace>/<scope>/config after initializing new cluster
# and all other cluster members will use it as a `global configuration`
bootstrap:
    dcs:
        ttl: 100
        loop_wait: 10
        retry_timeout: 10
        maximum_lag_on_failover: 1048576
        postgresql:
            use_pg_rewind: true
            use_slots: true
            parameters:
                   wal_level: replica
                   hot_standby: "on"
                   wal_keep_size: 8
                   max_wal_senders: 5
                   max_replication_slots: 5
                   checkpoint_timeout: 30

    initdb:
     - auth-host: md5
     - auth-local: peer
     - encoding: UTF8
     - data-checksums
     - locale: ru_RU.UTF-8
#     - locale: en_US.UTF-8

    # init pg_hba.conf
    pg_hba:
    - host replication postgres ::1/128 md5
    - host replication postgres 127.0.0.1/8 md5
    - host replication postgres 10.1.31.124/24 md5
    - host replication postgres 10.1.31.125/24 md5
    - host replication postgres 10.1.31.126/24 md5
    - host replication postgres 10.1.31.127/24 md5
    - host all all 0.0.0.0/0 md5

    users:
        admin:
            password: admin
            options:
                - createrole
                - createdb

postgresql:
    listen: 10.1.31.124:5432
    connect_address: 10.1.31.124:5432
    data_dir: /postgresql/data
    bin_dir:  /usr/lib/postgresql/14/bin
    pgpass: /tmp/pgpass
    authentication:
        replication:
            username: postgres
            password: postgres
        superuser:
            username: postgres
            password: postgres
    create_replica_methods:
        basebackup:
            checkpoint: 'fast'
    parameters:
        unix_socket_directories: '.'

tags:
    nofailover: false
    noloadbalance: false
    clonefrom: false
    nosync: false
```
для остальных, будет похожим, за исключением имени и ip

```commandline
scope: pgsql 
# должно быть одинаковым на всех нодах
namespace: /cluster/ 
# должно быть одинаковым на всех нодах
name: VM1 
# должно быть разным на всех нодах
 
restapi:
    listen: 10.1.31.124:8008 
    # адрес той ноды, в которой находится этот файл
    connect_address: 10.1.31.124:8008 
    # адрес той ноды, в которой находится этот файл
 
etcd:
    hosts: 10.1.31.124:2379,10.1.31.125:2379,10.1.31.126:2379,10.1.31.127:2379,10.1.31.128:2379 
# перечислите здесь все ваши ноды, в случае если вы устанавливаете etcd на них же
 
# this section (bootstrap) will be written into Etcd:/<namespace>/<scope>/config after initializing new cluster
# and all other cluster members will use it as a `global configuration`
bootstrap:
    dcs:
        ttl: 100
        loop_wait: 10
        retry_timeout: 10
        maximum_lag_on_failover: 1048576
        postgresql:
            use_pg_rewind: true
            use_slots: true
            parameters:
                   wal_level: replica
                   hot_standby: "on"
                   wal_keep_size: 8
                   max_wal_senders: 5
                   max_replication_slots: 5
                   checkpoint_timeout: 30
 
    initdb:
     - auth-host: md5
     - auth-local: peer
     - encoding: UTF8
     - data-checksums
     - locale: ru_RU.UTF-8
#     - locale: en_US.UTF-8
 
# init pg_hba.conf должен содержать адреса ВСЕХ машин, используемых в кластере
    pg_hba:
    - host replication postgres ::1/128 md5
    - host replication postgres 127.0.0.1/8 md5
    - host replication postgres 10.1.31.124/24 md5
    - host replication postgres 10.1.31.125/24 md5
    - host replication postgres 10.1.31.126/24 md5
    - host replication postgres 10.1.31.127/24 md5
    - host replication postgres 10.1.31.128/24 md5
    - host all all 0.0.0.0/0 md5
 
    users:
        admin:
            password: admin
            options:
                - createrole
                - createdb
 
postgresql:
    listen: 10.1.31.124:5432 
# адрес той ноды, в которой находится этот файл
    connect_address: 10.1.31.124:5432 
# адрес той ноды, в которой находится этот файл
    data_dir: /postgresql/data 
# эту директорию создаст скрипт, описанный выше и установит нужные права
    bin_dir:  /usr/lib/postgresql/14/bin 
# укажите путь до вашей директории с postgresql
    pgpass: /tmp/pgpass
    authentication:
        replication:
            username: postgres
            password: postgres
        superuser:
            username: postgres
            password: postgres
    create_replica_methods:
        basebackup:
            checkpoint: 'fast'
    parameters:
        unix_socket_directories: '.'
 
tags:
    nofailover: false
    noloadbalance: false
    clonefrom: false
    nosync: false
```

далее займёмся нашим демоном
```commandline
nano /etc/systemd/system/patroni.service 
```
```commandline
[Unit]
Description=Runners to orchestrate a high-availability PostgreSQL
After=syslog.target network.target
[Service]
Type=simple
User=postgres
Group=postgres
ExecStart=/usr/local/bin/patroni /etc/patroni/patroni.yml
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=process
TimeoutSec=30
Restart=no
[Install]
WantedBy=multi-user.target
```

```commandline
systemctl daemon-reload 
systemctl enable patroni
systemctl start patroni 
```
глянем что там у нас
```commandline
patronictl -c /etc/patroni/patroni.yml list
```
![img_2.png](img/img_2.png)

красота, 1 лидер и 3 реплики

на этом Patroni закончим

## Haproxy
идём на нашу VM5

```commandline
apt install haproxy 
```
и правим конфиг
```commandline
mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.conf.def 
nano /etc/haproxy/haproxy.cfg
```
```commandline
global
 maxconn 100

defaults
 log global
 mode tcp
 retries 2
 timeout client 30m
 timeout connect 4s
 timeout server 30m
 timeout check 5s

listen stats
 mode http
 bind *:7000
 stats enable
 stats uri /

listen postgres
 bind *:5432
 option httpchk
 http-check expect status 200
 default-server inter 4s fastinter 1s fall 3 rise 3 on-marked-down shutdown-sessions
 server VM1 10.1.31.124:5432 maxconn 100 check port 8008
 server VM2 10.1.31.125:5432 maxconn 100 check port 8008
 server VM3 10.1.31.126:5432 maxconn 100 check port 8008
 server VM4 10.1.31.127:5432 maxconn 100 check port 8008
```

инициализируем демона
```commandline
systemctl enable haproxy.service 
systemctl restart haproxy 
```
ну и глянем что происходит у нас на нашей веб морде

![img_3.png](img/img_3.png)

красота, на этом в принципе закончено

теперь у нас есть единая точка входа, это VM5

натравим на него сервер 1С, создадим пустую базу и в PG_Admin глянем что и как там

![img_4.png](img/img_4.png)

![img_5.png](img/img_5.png)

всё красиво

теперь проведём эксперемент, затушим работающую ноду, у нас это сейчас VM4, простой командой, просто остановим Patroni

идём на VM4
```commandline
systemctl stop patroni
```
посмотрим что у нас вышло

![img_6.png](img/img_6.png)

ага, 4 машина остановлена, а лидер переключился на 3

глянем на вебке

![img_7.png](img/img_7.png)

да, тут тоже всё отображается

проверим всё ли нормально, откроем на pgadmin

![img_8.png](img/img_8.png)

да, тут тоже проблем нет


### Итоги

Отказоустойчивый кластер построен, конечно мы не вдавались в подробности настройки конфига самого постгреса, он правится в конфиге патрони

Бакап можно снимать с любой ноды


